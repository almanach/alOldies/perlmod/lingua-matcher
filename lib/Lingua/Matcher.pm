# $Id$
package Lingua::Matcher;

=head1 NAME

Lingua::Matcher - Data-based named entity matcher

=head1 VERSION

Version 0.1

=head1 DESCRIPTION

This module allows to assemble matchers based on regular expressions and
instanciated from data

=head1 SYNOPSIS

    use Lingua::Matcher;

    my $build = sub {
      my ($string) = @_;
      my $pattern = "(?i)(?:� )?(?:$value (?:(?:-|x|ou|�) )?)*$value $string";
      return $pattern . '\b';
    };

    my $correct = sub {
      my ($string) = @_;
      $string =~ tr/Ilio�ga^/11100942/;
      return $string;
    };

   my $matcher = Lingua::Matcher->new(
      'DIMENSION',
      $data ? $data : \*DATA,
      1,
      $build,
      ($no_sw ? undef : $correct)
   );

   $matcher->run();
   __DATA__
   m
   mm
   cm
   km

=cut

use strict;
use warnings;
use Carp;
use locale;
use Regexp::Assemble;
use File::Basename;

our $VERSION = '0.1';

=head1 CLASS METHODS

=head2 new($tag, $data, $fuzzy, $build, $correct)

Creates and return a new Lingua::Matcher object

=cut

sub new {
    my ($class, $tag, $data, $fuzzy, $build, $correct) = @_;

    croak "No tag given, aborting" unless $tag;
    croak "No data given, aborting" unless $data;

    my $re;
    my $compiled_data;

    unless (ref $data && ref $data eq 'GLOB') {
        croak "Non-existant data file given, aborting" unless -f $data;
        $compiled_data = basename($data) . '.comp';
        if (
            -f $compiled_data &&
            (stat($compiled_data))[9] > (stat($data))[9]
        )  {
            open DATA, "<$compiled_data" ||  die "can't open names file $compiled_data: $!";
            my $string = <DATA>;
            chomp $string;
            $re = qr/$string/;
            close DATA;
        }
    }

    unless ($re) {
        my $ra = Regexp::Assemble->new(
            reduce  => 1,
            mutable => 0
        );

        unless (ref $data && ref $data eq 'GLOB') {
            open DATA, "<$data" ||  die "can't open names file $data: $!";
        } else {
            *DATA = $data;
        }

        while (<DATA>) {
            next if /^#/;
            chomp;

            # protect metacharacters
            s/([().])/\\$1/g;

            my $pattern;
            if ($build) {
                $pattern = $build->($_);
            } else {
                $pattern = $_;
            }

            # allow fuzzy spaces number
            $pattern =~ s/ / */g if $fuzzy;

            $ra->add($pattern);
        }

        unless (ref $data && ref $data eq 'GLOB') {
            close DATA;

            open DATA, ">$compiled_data" ||  die "can't open names file $compiled_data: $!";
            print DATA $ra->as_string();
            close DATA;
        }

        $re = $ra->re();
    }

    my $self = bless {
        _tag     => $tag,
        _re      => $re,
        _correct => $correct,
    }, $class;

    return $self;
}

=head1 INSTANCE METHODS

=head2 run

run the matcher object on STDIN.

=cut

sub run {
    my ($self) = @_;

    while (<>) {
        if ($self->{_correct}) {
            s/($self->{_re})/'{' . $self->{_correct}->($1) . '}_' . $self->{_tag} . ' '/eg;
        } else {
            s/($self->{_re})/'{' . $1 . '}_' . $self->{_tag} . ' '/g;
        }
        print;
    }
}

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2006, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Guillaume Rousse <grousse@cpan.org>

=cut


1;
